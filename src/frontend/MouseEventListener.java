package frontend;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import backend.Move;
import backend.Pieces.Ant;
import backend.Pieces.Beetle;
import backend.Pieces.Hopper;
import backend.Pieces.Piece;
import backend.Pieces.Queen_bee;
import backend.Pieces.Spider;


public class MouseEventListener extends MouseAdapter{

	public int i;
	public int j;
	private static int x,y;
	@Override
	public void mouseClicked(MouseEvent arg0) {
		int i, j;
		i = (arg0.getY()-31)/48;
		if(i%2==0)
			j=(arg0.getX()-Hive_Drawer.LEFT_BAR_SEPARATION-12)/56;
		else
			j=(arg0.getX()-Hive_Drawer.LEFT_BAR_SEPARATION-40)/56;

		x=i;
		y=j;
		System.out.println(i+","+j);
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
	}

	@Override
	public void mousePressed(MouseEvent arg0) {
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
	}

	public static Move translateToHumanMove(int team){
		int THREAD_SLEEP_TIME=100;
		
		
		int x0,y0,x1,y1,ax,ay;
		System.out.println("Primera seleccion");
		ax=x;
		ay=y;
		while(ax==x && ay==y){
			try {
				Thread.sleep(THREAD_SLEEP_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("Primera seleccion hecha: "+x+","+y);
		x0=x;
		y0=y;
		ax=x;
		ay=y;
		System.out.println("Segunda seleccion");
		while(ax==x && ay==y){
			try {
				Thread.sleep(THREAD_SLEEP_TIME);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		System.out.println("Segunda seleccion hecha: "+x+","+y);

		x1=x;
		y1=y;

		if(y0==-1){
			Piece piece=getSideBarPiece(x0,x1,y1,team);
			System.out.println(piece+" en "+x1+y1);
			return new Move(x1, y1, piece);
		}else{
			return new Move(x0, y0, x1,y1);
		}
	}

	private static Piece getSideBarPiece(int x0,int x, int y, int team) {
		switch(x0){
		case 1: return new Ant(x,y,team);
		case 2: return new Beetle(x,y,team);
		case 4: return new Hopper(x,y,team);
		case 5: return new Queen_bee(x,y,team);
		case 6: return new Spider(x,y,team);
		}
		return null;
	}

}
