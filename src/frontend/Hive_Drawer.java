package frontend;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import backend.Pieces.Ant;
import backend.Pieces.Beetle;
import backend.Pieces.BoardPiece;
import backend.Pieces.Hopper;
import backend.Pieces.Piece;
import backend.Pieces.Queen_bee;
import backend.Pieces.Spider;


public class Hive_Drawer extends JFrame{

	static int PIECE_SIDE=64;
	static int PIECE_SEPARATION_HEIGHT=48;
	static int PIECE_SEPARATION_WIDTH=56;
	static int SCREEN_HEIGH=PIECE_SEPARATION_HEIGHT*(GameManager.BOARD_SIDE+1);
	static int SCREEN_WIDTH=PIECE_SEPARATION_WIDTH*(GameManager.BOARD_SIDE+1);
	static int LEFT_BAR_SEPARATION=64*2;

	static Map<Class,String> iconos;
	private JPanel pieces;

	private boolean showBoardMarks;
	private boolean isometricView;
	
	
	private static Set<JLabel> sideBarPieces;

	public Hive_Drawer(boolean showBoardMarks,boolean isometricView){
		this.showBoardMarks=showBoardMarks;
		this.isometricView=isometricView;
		if(isometricView){
			SCREEN_HEIGH*=2;
			SCREEN_WIDTH*=2;
		}

		this.setLayout(null);
		this.setResizable(false);
		getContentPane().setPreferredSize(new Dimension(SCREEN_WIDTH+LEFT_BAR_SEPARATION*2,
				SCREEN_HEIGH));
		pack();
		setVisible(true);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);

		addMouseListener(new MouseEventListener());

		pieces=new JPanel();
		pieces.setSize(SCREEN_WIDTH+LEFT_BAR_SEPARATION*2,SCREEN_HEIGH);
		pieces.setVisible(true);
		pieces.setBackground(Color.BLACK);
		pieces.setOpaque(true);
		pieces.setLayout(null);
		getContentPane().add(pieces);

		iconos=new HashMap<Class,String>();
		initializeIconMap();
		initializeSideBar();
	}

	private void initializeSideBar() {
		sideBarPieces=new HashSet<JLabel>();
		int i=0;
		sideBarPieces.add(drawPieceFlat(new Ant(0,0,0), 32+(i++)*64, 32));
		sideBarPieces.add(drawPieceFlat(new Beetle(0,0,0), 32+(i++)*64, 32));
		sideBarPieces.add(drawPieceFlat(new Hopper(0,0,0), 32+(i++)*64, 32));
		sideBarPieces.add(drawPieceFlat(new Queen_bee(0,0,0), 32+(i++)*64, 32));
		sideBarPieces.add(drawPieceFlat(new Spider(0,0,0), 32+(i++)*64, 32));
	}

	public void drawBoard(Piece[][] board,int team){
		pieces.removeAll();
		for(Component c:sideBarPieces){
			pieces.add(c);
		}
		for(int i=board.length-1;i>=0;i--){
			for(int j=board[0].length-1;j>=0;j--){
				Piece iterador=board[i][j];
				while(iterador.getOnTop()!=null){
					iterador=iterador.getOnTop();
				}
				if(isometricView){
					pieces.add(drawPieceIsometric(iterador,i,j));
				}else{
					pieces.add(drawPieceFlat(iterador));
				}
			}
		}
		pieces.add(drawMetagameData(team));
		repaint();
	}

	private JLabel drawMetagameData(int team) {
		JLabel newLayer=new JLabel();
		newLayer.setSize(PIECE_SIDE*2, PIECE_SIDE*3);
		newLayer.setVisible(true);
		newLayer.setLocation((int) (LEFT_BAR_SEPARATION+GameManager.boardSize*(PIECE_SEPARATION_WIDTH+2.5)),(GameManager.boardSize*(PIECE_SEPARATION_HEIGHT-15)));

		
		newLayer.setText("<HTML><h1><font color=WHITE> Mueve el <br> jugador "+team+"</font></h1></HTML>");
		
		return newLayer;
	}

	private JLabel drawPieceIsometric(Piece piece, int jCart, int iCart) {
		int i=iCart-jCart;
		int j=(iCart+iCart)/2;

		JLabel newPiece=new JLabel();
		newPiece.setSize(PIECE_SIDE, PIECE_SIDE);
		newPiece.setVisible(true);
		newPiece.setLocation((i*64-32*j)*(-1)+64*6,j*16+64*6);

		ImageIcon icono=null;
		if(piece instanceof BoardPiece){
			icono= new ImageIcon(Hive_Drawer.class.getClassLoader().getResource("frontend/sprites/iso_ground.png"));
		}
		else{
			icono= new ImageIcon(Hive_Drawer.class.getClassLoader().getResource("frontend/sprites/iso_wall.png"));
		}
		newPiece.setIcon(icono);

		if(showBoardMarks){
			newPiece.setHorizontalTextPosition(JLabel.CENTER);

			String[] classNames=piece.getClass().getCanonicalName().split("\\.");
			String classSimpleName=classNames[classNames.length-1];
			if(classSimpleName.equals("BoardPiece")){
				classSimpleName="";
			}

			newPiece.setText("<HTML><div style=\"text-align: center;\"><font color=LIME size=5>"+jCart+","+iCart+
					"<br></font><font color=YELLOW size=2>"+classSimpleName+"</font></div></HTML>");
		}

		return newPiece;
	}

	private JLabel drawPieceFlat(Piece piece, int i, int j) {
		JLabel newPiece=new JLabel();
		newPiece.setSize(PIECE_SIDE, PIECE_SIDE);
		newPiece.setVisible(true);
		newPiece.setLocation(j,i);
		ImageIcon icono= new ImageIcon(Hive_Drawer.class.getClassLoader().getResource(iconos.get(piece.getClass())));
		newPiece.setIcon(icono);

		if(showBoardMarks){
			newPiece.setHorizontalTextPosition(JLabel.CENTER);

			String[] classNames=piece.getClass().getCanonicalName().split("\\.");
			String classSimpleName=classNames[classNames.length-1];
			newPiece.setText("<HTML><div style=\"text-align: center;\"><font color=YELLOW size=3>"+classSimpleName+"</font></div></HTML>");
		}

		return newPiece;
	}

	private JLabel drawPieceFlat(Piece piece) {
		JLabel newPiece=new JLabel();
		newPiece.setSize(PIECE_SIDE, PIECE_SIDE);
		newPiece.setVisible(true);
		newPiece.setLocation(LEFT_BAR_SEPARATION+piece.y*PIECE_SEPARATION_WIDTH+(int)(((double)(piece.x%2)/2)*PIECE_SEPARATION_WIDTH),(piece.x*PIECE_SEPARATION_HEIGHT));
		ImageIcon icono= new ImageIcon(Hive_Drawer.class.getClassLoader().getResource(iconos.get(piece.getClass())));
		newPiece.setIcon(icono);

		if(showBoardMarks){
			newPiece.setHorizontalTextPosition(JLabel.CENTER);

			String[] classNames=piece.getClass().getCanonicalName().split("\\.");
			String classSimpleName=classNames[classNames.length-1];
			String team=""+piece.team;
			if(classSimpleName.equals("BoardPiece")){
				classSimpleName="";
				team="";
			}
			newPiece.setText("<HTML><div style=\"text-align: center;\"><font color=MAROON size=4>"+ team+"</font><br><font color=LIME size=5>"+piece.x+","+piece.y+
					"<br></font><font color=YELLOW size=2>"+classSimpleName+"</font></div></HTML>");
		}

		return newPiece;
	}

	
	private void initializeIconMap() {
		iconos.put(BoardPiece.class,"frontend/sprites/51306.png");
		iconos.put(Ant.class,"frontend/sprites/hexagon-outline-xl.png");
		iconos.put(Hopper.class,"frontend/sprites/hexagon-xl (1).png");
		iconos.put(Queen_bee.class,"frontend/sprites/hexagon-xl (2).png");
		iconos.put(Spider.class,"frontend/sprites/hexagon-xl (3).png");
		iconos.put(Beetle.class,"frontend/sprites/hexagon-xl.png");
	}

}
