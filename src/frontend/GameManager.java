package frontend;

import backend.Move;
import backend.Player;
import backend.Pieces.BoardPiece;
import backend.Pieces.Piece;
import backend.Pieces.Queen_bee;
import backend.players.HumarPlayer;
import backend.players.RandomPlayer;

public class GameManager {
	public static BoardPiece[][] board;
	static final boolean SHOW_BOARD_MARKS=true;
	static final boolean ISOMETRIC_VIEW=false;
	public static final int BOARD_SIDE = 8;


	public static final int MAX_TURNS_WITHOUT_QUEEN = 3;
	static int boardSize;
	static Hive_Drawer drawer;

	static int currentTurn;
	static Queen_bee[] queens;


	public static void main(String[] args) {
		currentTurn=0;
		queens=new Queen_bee[3];
		boardSize=BOARD_SIDE;
		board=initializeBoard(BOARD_SIDE);
		drawer=new Hive_Drawer(SHOW_BOARD_MARKS,ISOMETRIC_VIEW);
		boolean playing = true, turn = true;
		Player player1, player2;		

		player1=new HumarPlayer(1);
		player2=new RandomPlayer(2);

		drawer.drawBoard(board,1);

		while(playing){
			boolean moved;
			if(turn){
				System.out.println("Mueve jugador 1");			
				moved=move(player1.getMove(board),player1.team);			
			}else{
				System.out.println("Mueve jugador 2");
				moved=move(player2.getMove(board),player2.team);
			}
			playing = !didIWin();
			if(moved){
				turn=!turn;
				drawer.drawBoard(board,(turn)?1:2);
				currentTurn++;
			}
		}
	}

	private static boolean didIWin() {
		boolean firstTrapped=false,secondTrapped=false;
		if(isTrapped(1)){
			System.out.println("Reina 1 atrapada!");
			firstTrapped=true;
		}
		if(isTrapped(2)){
			System.out.println("Reina 2 atrapada!");
			secondTrapped=true;
		}
		return firstTrapped||secondTrapped;
	}

	private static boolean isTrapped(int team){
		if(queens[team]==null){
			return false;
		}
		Queen_bee queen=queens[team];
		for(Piece neighbor:board[queen.x][queen.y].getSurround()){
			if(neighbor!=null && Piece.getUpperPiece(neighbor.x, neighbor.y, board) instanceof BoardPiece){
				return false;
			}
		}
		return true;
	}

	private static boolean move(Move move,int team) {
		boolean ableToMove=false;

		if(move.isNormalMove()){
			if(queens[team]==null){
				System.out.println("No podes mover hasta tener una reina");
				return false;
			}			
			Piece movingPiece=Piece.getUpperPiece(move.getX1(), move.getY1(), board);
			if(movingPiece!=null && movingPiece.team!=team){
				System.out.println("Moviendo una pieza que no es tuya");
				return false;
			}
			ableToMove=board[move.getX1()][move.getY1()].move(move.getX2(), move.getY2(),board);
		}else{
			if(queens[team]==null && currentTurn/2>=MAX_TURNS_WITHOUT_QUEEN
					&& !(move.getPiece() instanceof Queen_bee)){
				System.out.println("Tenes que poner una reina");
				return false;
			}
			ableToMove=place(move.getX1(), move.getY1(), move.getPiece());
		}

		if(!ableToMove){
			System.out.println("Movimiento invalido "+move);
		}
		return ableToMove;
	}

	//SOLO PARA DEBUGGEAR, despues hacelo privado. Gracias =)
	public static boolean place(int x1, int y1, Piece piece) {
		if(board[x1][y1].getOnTop()!=null)
			return false;
		boolean firstPlace=false,secondPlace=false;
		if(currentTurn==0){
			firstPlace=true;
		}else if(currentTurn==1){
			for(Piece neighbor:board[x1][y1].getSurround()){
				if(neighbor!=null && !(Piece.getUpperPiece(neighbor.x,neighbor.y, board) instanceof BoardPiece)){
					secondPlace=true;
				}
			}			
		}
		if(!firstPlace && !secondPlace){
			boolean teamLink=false;
			for(Piece neighbor:board[x1][y1].getSurround()){
				Piece aux=null;
				if(neighbor!=null && !((aux=Piece.getUpperPiece(neighbor.x,neighbor.y, board)) instanceof BoardPiece)){
					if(aux!=null && piece!=null && aux.team==piece.team){
						teamLink=true;
					}else{
						return false;
					}
				}
			}
			if(!teamLink){
				return false;
			}
		}
		// Legaste hasta aca, es porque podes hacer el place
		if(piece instanceof Queen_bee){
			if(queens[piece.team]==null)
				queens[piece.team]=(Queen_bee)piece;
			else{
				System.out.println("Solo una reina por equipo");
				return false;
			}
		}
		board[x1][y1].setOnTop(piece);	
		return true;
	}

	private static BoardPiece[][] initializeBoard(int boardSide) {
		board=new BoardPiece[boardSide][boardSide];
		boardSize = boardSide;
		for(int i = 0; i< boardSize; i++){
			for(int j = 0; j< boardSize; j++){
				board[i][j] = new BoardPiece(i,j);
			}
		}
		for(int i = 0; i< boardSize; i++){
			for(int j = 0; j< boardSize; j++){
				updateSurrounds(i,j);
			}
		}
		return board;
	}

	private static void updateSurrounds(int i, int j) {
		if(i%2==0){
			if(i!=0 && j!=0)
				board[i][j].getSurround()[0] = board[i-1][j-1];

			if(i!=boardSize-1 && j!=0)
				board[i][j].getSurround()[3] = board[i+1][j-1];

			if(j!=0)
				board[i][j].getSurround()[1] = board[i][j-1];

			if(j!=boardSize-1)
				board[i][j].getSurround()[2] = board[i][j+1];

			if(i!=boardSize-1)
				board[i][j].getSurround()[4] = board[i+1][j];

			if(i!=0)	
				board[i][j].getSurround()[5] = board[i-1][j];

		}else{
			if(i!=0 && j!=boardSize-1)
				board[i][j].getSurround()[5] = board[i-1][j+1];

			if(i!=boardSize-1 && j!=boardSize-1)
				board[i][j].getSurround()[4] = board[i+1][j+1];

			if(j!=0)
				board[i][j].getSurround()[1] = board[i][j-1];

			if(j!=boardSize-1)
				board[i][j].getSurround()[2] = board[i][j+1];

			if(i!=boardSize-1)
				board[i][j].getSurround()[3] = board[i+1][j];

			if(i!=0)	
				board[i][j].getSurround()[0] = board[i-1][j];
		}


	}



}
