package backend.Pieces;

public class Ant extends Piece{

	public Ant(int x, int y, int team) {
		super(x, y,team);
	}
	
	@Override
	public boolean move(int x0, int y0,final BoardPiece[][] board) {
		if(fitFromHere(x0, y0, board)){
			return jump(x0,y0,board);
		}
		return false;
	}

}
