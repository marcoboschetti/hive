package backend.Pieces;

import java.util.HashSet;
import java.util.Set;


public class Spider extends Piece{

	private static int MAX_MOVES=3;
	
	public Spider(int x, int y, int team) {
		super(x, y,team);
	}

	@Override
	public boolean move(int x, int y,final BoardPiece[][] board) {
		Set<Piece> aux=new HashSet<Piece>();
		if(canMoveR(this.x,this.y,x,y,board,MAX_MOVES,aux)){
			return jump(x,y,board);
		}	
		return false;
	}

	private boolean canMoveR(int curx, int cury,int x, int y, BoardPiece[][] board, int remainingJumps,Set<Piece> set) {
		if(remainingJumps==0){
			return false;
		}
		
		set.add(board[curx][cury]);
		for(Piece piece:board[curx][cury].surround){
			if(piece!=null && !set.contains(piece)){
				if(piece.x==x && piece.y==y && getUpperPiece(piece.x,piece.y,board) instanceof BoardPiece)
					return true;
				if(canMoveR(piece.x, piece.y, x, y, board, remainingJumps-1, set)){
					return true;
				}
			}
		}		
		set.remove(board[curx][cury]);			

		return false;
	}


}
