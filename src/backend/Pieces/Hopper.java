package backend.Pieces;


public class Hopper extends Piece{

	public Hopper(int x, int y, int team) {
		super(x, y,team);
	}

	@Override
	public boolean move(int x0, int y0,final BoardPiece[][] board) {
		if(canMoveTo(x0, y0,board)){
			return jump(x0,y0,board);
		}
		return false;
	}

	private boolean canMoveTo(int x0, int y0,BoardPiece[][] board){
		for(int i=0;i<6;i++){
			BoardPiece iterator=board[x][y];
			while(iterator!=null && !(Piece.getUpperPiece(iterator.x,iterator.y,board) instanceof BoardPiece)){
				iterator=iterator.getSurround()[i];
			}
			if(iterator!=null && iterator.x==x0 && iterator.y==y0){
				return true;
			}
		}
		return false;
	}

}
