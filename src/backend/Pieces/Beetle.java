package backend.Pieces;

public class Beetle extends Piece{

	public Beetle(int x, int y, int team) {
		super(x, y,team);
	}

	public boolean move(int x0, int y0,final BoardPiece[][] board) {
		for(BoardPiece neighbor:board[this.x][this.y].surround){
			if(neighbor!=null){
				System.out.println(neighbor.x +" , "+neighbor.y);
				if(neighbor.x==x0 && neighbor.y==y0){
					return jump(x0, y0, board);
				}
			}
		}
		return false;
	}

}
