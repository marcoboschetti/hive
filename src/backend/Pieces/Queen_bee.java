package backend.Pieces;


public class Queen_bee extends Piece{

	public Queen_bee(int x, int y, int team) {
		super(x, y,team);
		// TODO Auto-generated constructor stub
	}

	@Override
	public boolean move(int x0, int y0,final BoardPiece[][] board) {
		for(Piece neighbor:board[x][y].surround){
			if(neighbor!=null && neighbor.x==x0 && neighbor.y==y0){
				if(fitFromHere(x0, y0, board)){
					return jump(x0, y0, board);
				}else{
					return false;
				}
				
			}
		}
		return false;
	}


}
