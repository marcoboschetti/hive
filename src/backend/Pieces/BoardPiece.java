package backend.Pieces;



public class BoardPiece extends Piece{
	BoardPiece[] surround;
	
	public BoardPiece(int x, int y) {
		super(x, y,0);
		surround=new BoardPiece[6];
	}

	public BoardPiece[] getSurround() {
		return surround;
	}
	
	public void setSurround(BoardPiece[] surround) {
		this.surround = surround;
	}
	
	@Override
	public boolean move(int x, int y, BoardPiece[][] board) {
		if(onTop!=null){
			Piece iterator=onTop;
			while(iterator.onTop!=null){
				iterator=iterator.onTop;
			}
			return iterator.move(x, y, board);
		}
		return false;
	}
	
	public void setSurround(Piece[] surround) {
	// Gran nombre para una exception. no sabia que existia
		throw new IllegalArgumentException("Should not modify BoardPiece sorrund!");
	}
	
	public boolean equals(Object obj){
		if(!(obj instanceof BoardPiece))
			return false;
		BoardPiece aux = (BoardPiece) obj;
		return aux.x == x && aux.y == y;
	}
}
