package backend.Pieces;

import java.util.HashSet;
import java.util.Set;


public abstract class Piece {

	Piece onTop;
	public int x,y;
	public int team;
	
	public Piece(int x,int y, int team){
		this.x=x;
		this.y=y;
		this.team=team;
	}

	public Piece getOnTop() {
		return onTop;
	}

	public void setOnTop(Piece onTop) {
		this.onTop = onTop;
	}	
	public abstract boolean move(int x, int y,final BoardPiece[][] board);

	//public abstract boolean special();

	/** Deberia ser el UNICO que pueda tocar el tablero **/
	// Metodo generico para saltar de un lugar a otro... lo usan todas por ahora
	// NO VALIDA NADA! El checkeo posta lo hace cada pieza.
	protected boolean jump(int x0, int y0, BoardPiece[][] board){
		if(!conexo(x0,y0,x,y,board)){
			System.out.println("ROMPE REGLA DE CONEXIDAD");
			return false;
		}
		Piece iterator=board[x][y];
		while(iterator.onTop!=this){
			iterator=iterator.onTop;
		}
		iterator.onTop=null;
		this.x=x0;
		this.y=y0;
		iterator=board[x0][y0];
		while(iterator.onTop!=null){
			iterator=iterator.onTop;
		}
		iterator.onTop=this;
		return true;
	}


	// Trata de llegar desde todos los vecinos de this entre s�, habilitando x0-y0	
	private static boolean conexo(int xadded, int yadded,int xremoved,int yremoved,final BoardPiece[][] board) {
		if(hasSomethingBelow(xremoved,yremoved,board))
			return true;
		//veo si la nueva posicion queda linkeada a algo
		boolean newLocationLinked=false;
		for(Piece p:board[xadded][yadded].surround){
			if(p!=null){
				Piece upperPiece=getUpperPiece(p.x, p.y, board);
				Piece self=getUpperPiece(xremoved, yremoved, board);
				if(upperPiece!=null && upperPiece!=self && !(upperPiece instanceof BoardPiece)){
					newLocationLinked=true;
				}
			}
		}
		if(!newLocationLinked){
			return false;
		}

		for(int i=0;i<5;i++){
			Piece neighbor=board[xremoved][yremoved].surround[i];
			if(neighbor!=null && !(getUpperPiece(neighbor.x, neighbor.y, board) instanceof BoardPiece)){
				for(Piece target:board[xremoved][yremoved].surround){
					if(target!=null && target!=neighbor && !(getUpperPiece(target.x, target.y, board) instanceof BoardPiece) ){
						Set<Piece> used=new HashSet<Piece>();
						used.add(board[xremoved][yremoved]);
						if(!hasAPath(neighbor,target,board,xadded,yadded,used)){
							return false;
						}
					}
				}
			}			
		}		
		return true;
	}

	private static boolean hasSomethingBelow(int xremoved, int yremoved,
			BoardPiece[][] board) {
		BoardPiece boardPiece=board[xremoved][yremoved];
		if(boardPiece.onTop!=null && boardPiece.onTop!=getUpperPiece(xremoved,yremoved, board)){
			return true;
		}
		return false;
	}

	//Trata de llegar desde source a target pasando por piezas existentes
	//Lepo: Esta contemplando la nueva pieza agregada :)
	private static boolean hasAPath(Piece source, Piece target,
			BoardPiece[][] board, int xadded, int yadded, Set<Piece> used) {
		if(source==target){
			return true;
		}
		if(getUpperPiece(source.x, source.y, board) instanceof BoardPiece){
			return false;
		}
		BoardPiece current=board[source.x][source.y];
		used.add(current);
		for(Piece neighbor:current.surround){
			if(neighbor!=null && !used.contains(neighbor)){
				if(hasAPath(neighbor, target, board, xadded, yadded, used)){
					return true;
				}
			}
		}
		// Mi ultima chance es pasando por donde va a estar la nueva pieza. REGLA DE LEPO
		if(!used.contains(board[xadded][yadded]) && hasAPath(board[xadded][yadded], target, board, xadded, yadded, used)){
			return true;
		}

		return false;
	}

	public static Piece getUpperPiece(int x, int y, BoardPiece[][] board){
		Piece iterator=board[x][y];
		while(iterator.onTop!=null){
			iterator=iterator.onTop;
		}
		return iterator;
	}

	protected boolean fitFromHere(int targetX, int targetY, BoardPiece[][] board) {
		return fit(targetX, targetY, board, this.x, this.y);
	}

	protected boolean fit(int targetX, int targetY, BoardPiece[][] board, 
			int currentX, int currentY){
		return fitR(targetX, targetY, board, currentX, currentY, new HashSet<BoardPiece>());
	}

	private boolean fitR(int x0, int y0, BoardPiece[][] board, 
			int currentX, int currentY, Set<BoardPiece> set){
		int count;
		BoardPiece current = board[currentX][currentY];
		if(current.x == x0 && current.y == y0)
			return true;
		set.add(current);
		for(BoardPiece target: current.surround){
			if(target!=null && target.getOnTop() == null && !set.contains(target)){
				count = 0;
				for(BoardPiece neighbor: target.getSurround()){	
					if(neighbor!=null)
						for(BoardPiece piece: current.surround){
							if(piece != null && piece.getOnTop()!=null && piece.equals(neighbor)){
								count++;
							}
						}
				}
				if(count<2){
					if(fitR(x0, y0, board, target.x, target.y, set))
						return true;
				}

			}
		}
		return false;
	}
	
	public String toString(){
		return "("+getClass()+"-"+x+","+y+")";
	}
}
