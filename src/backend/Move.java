package backend;

import backend.Pieces.Piece;


public class Move {
	private boolean normalMove;
	private int x1, x2, y1, y2;
	private Piece piece;

	public Move(int x1, int y1, int x2, int y2){
		this.normalMove = true;
		this.x1 = x1;
		this.x2 = x2;
		this.y1 = y1;
		this.y2 = y2;
	}

	public Move(int x1, int y1, Piece piece){
		this.normalMove = false;
		this.x1 = x1;
		this.y1 = y1;
		this.piece = piece;
	}

	public boolean isNormalMove() {
		return normalMove;
	}

	public int getX1() {
		return x1;
	}

	public int getX2() {
		return x2;
	}

	public int getY1() {
		return y1;
	}

	public int getY2() {
		return y2;
	}

	public Piece getPiece(){
		return this.piece;
	}

	public String toString(){
		return "("+x1+","+y1+","+x2+","+y2+")";
	}
}
