package backend;

import backend.Pieces.BoardPiece;


public abstract class Player {
	public int team;
	
	public abstract Move getMove(BoardPiece[][] board);
}
