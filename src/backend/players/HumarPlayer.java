package backend.players;

import backend.Move;
import backend.Player;
import backend.Pieces.BoardPiece;
import frontend.MouseEventListener;

public class HumarPlayer extends Player{


	public HumarPlayer(int team){
		this.team=team;
	}

	@Override
	public Move getMove(BoardPiece[][] board) {
		return MouseEventListener.translateToHumanMove(team);
	}
}
