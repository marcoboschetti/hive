package backend.players;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import backend.Move;
import backend.Player;
import backend.Pieces.Ant;
import backend.Pieces.Beetle;
import backend.Pieces.BoardPiece;
import backend.Pieces.Hopper;
import backend.Pieces.Piece;
import backend.Pieces.Queen_bee;
import backend.Pieces.Spider;
import frontend.GameManager;

public class RandomPlayer extends Player{


	private List<Piece> myPieces;
	public RandomPlayer(int team){
		this.team=team;
		myPieces=new LinkedList<Piece>();
	}

	@Override
	public Move getMove(BoardPiece[][] board) {
		Set<Piece> aux=new HashSet<Piece>();
		for(Piece piece:myPieces){
			if(Piece.getUpperPiece(piece.x, piece.y, board)!=piece){
				aux.add(piece);
			}
		}
		myPieces.removeAll(aux);


		int rand=(int) (Math.random()*100);
		System.out.println(rand+" vs "+myPieces.size()*20);

		if(rand>=myPieces.size()*20){
			Piece newPiece=getRandomPiece();
			System.out.println(newPiece);
			newPiece.x=(int) (Math.random()*GameManager.BOARD_SIDE);			
			newPiece.y=(int) (Math.random()*GameManager.BOARD_SIDE);			
			myPieces.add(newPiece);
			return new Move(newPiece.x,newPiece.y,newPiece);
		}
		Piece selected=myPieces.get((int)(Math.random()*myPieces.size()));
		return new Move(selected.x,selected.y, (int) (Math.random()*GameManager.BOARD_SIDE),(int) (Math.random()*GameManager.BOARD_SIDE));

	}

	private Piece getRandomPiece() {
		if(Math.random()<0.25*myPieces.size())
			return new Queen_bee(0,0,team);

		double rand=Math.random();
		if(rand<0.25)
			return new Ant(0,0,team);
		else if(rand<0.5)
			return new Beetle(0,0,team);
		else if(rand<0.75)
			return new Hopper(0,0,team);
		else
			return new Spider(0,0,team);
	}




}
